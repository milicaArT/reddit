﻿using System.Collections.Generic;

namespace Reddit.Domain.Entities
{
    public class Community : IEntity
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public List<long> Users { get; set; }
        public List<long> Posts { get; set; }
        public List<long> Messages { get; set; }
    }
}
