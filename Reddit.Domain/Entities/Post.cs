﻿using System.Collections.Generic;

namespace Reddit.Domain.Entities
{
    public class Post : IEntity
    {
        public long Id { get; set; }
        public long AuthorId { get; set; }
        public string Content { get; set; }
        public List<long> Likes { get; set; }
        public List<long> Dislikes { get; set; }
        public int LikesCount { get; set; }
        public List<long> Comments { get; set; }
        public long Community { get; set; }
        public string CommunityTitle { get; set; }
        public double TimeStamp { get; set; }
        public double Popularity { get; set; }
    }
}
