﻿using System;

namespace Reddit.Redis
{
    public class RedisOptions
    {
        public String Endpoint { get; set; }
        public String ClientName { get; set; }
    }
}
