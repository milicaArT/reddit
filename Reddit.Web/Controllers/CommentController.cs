﻿using Microsoft.AspNetCore.Mvc;
using Reddit.Business.CommentManagement;
using Reddit.Business.CommentManagement.Input;
using Reddit.Domain.Interop;
using Reddit.Services.Repositories.Comments;
using System.Threading.Tasks;

namespace Reddit.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [Produces("application/json")]
    public class CommentController : ControllerBase
    {
        private ICommentManager _commentManager;
        private ICommentRepository _commentRepository;

        public CommentController(
            ICommentManager commentManager,
            ICommentRepository commentRepository)
        {
            _commentManager = commentManager;
            _commentRepository = commentRepository;
        }

        [HttpPost]
        public async Task<IActionResult> ReplyToComment([FromBody]CreateReplyCommentInput input)
        {
            Result<long> result = await _commentManager.ReplyToComment(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> LikeComment(long commentId, long userId, string communityTitle)
        {
            Result result = await _commentManager.LikeComment(commentId, userId, communityTitle);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> DislikeComment(long commentId, long userId, string communityTitle)
        {
            Result result = await _commentManager.DislikeComment(commentId, userId, communityTitle);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }
    }
}
