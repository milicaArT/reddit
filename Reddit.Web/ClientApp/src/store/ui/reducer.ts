import { Reducer } from "redux";
import { getItemFromLocalStorage, LOGGED_USER_KEY } from "../../services/local-storage";
import { AppActionTypes, LoadCommunityDataAction, LoadDataAction } from "../app/types";
import { AddPostAction, LoadMorePostsAction, LoadPostsAction, PostActionTypes } from "../post/types";
import { LoadUserDataAction, UserActionTypes } from "../user/types";
import { OpenErrorDialogAction, SetLoggedUserAction, SortPostsAction, SelectChatCommunityAction } from "./action";
import { UiActionTypes, UiState } from "./types";

const initialState: UiState = {
  isOpenedSinglePost: false,
  loggedUser: 0,
  openedPostId: 0,
  homePosts: [],
  communityPosts: [],
  userPosts: [],
  isLoginDialogOpened: false,
  isSignupDialogOpened: false,
  postsSortType: "popular",
  error: false,
  errorMessage: "",
  errorTitle: "",
  spinner: false,
  selectedChatCommunity: 0,
  hubConnectionId: ""
}

const reducer: Reducer<UiState> = (state = initialState, action) => {
  switch (action.type) {
    case AppActionTypes.FETCH_DATA: {
      const loggedUser: number | null = getItemFromLocalStorage<number>(LOGGED_USER_KEY);
      if(loggedUser) return { ...state, loggedUser }
      else return state;
    }
    case UiActionTypes.SET_LOGGED_USER: {
      return {
        ...state, 
        loggedUser: (action as SetLoggedUserAction).user,
        homePosts: [],
        communityPosts: [],
        userPosts: []
      }
    }
    case UiActionTypes.LOGOUT_USER: {
      return {
        ...state,
        loggedUser: 0,
        homePosts: [],
        communityPosts: [],
        userPosts: []
      }
    }
    case UiActionTypes.OPEN_LOGIN_DIALOG: {
      return {...state, isLoginDialogOpened: true}
    }
    case UiActionTypes.CLOSE_LOGIN_DIALOG: {
      return {...state, isLoginDialogOpened: false}
    }
    case UiActionTypes.OPEN_SIGNUP_DIALOG: {
      return {...state, isSignupDialogOpened: true}
    }
    case UiActionTypes.CLOSE_SIGNUP_DIALOG: {
      return {...state, isSignupDialogOpened: false}
    }
    case UiActionTypes.INIT_SORT_POSTS: {
      return {
        ...state, 
        postsSortType: (action as SortPostsAction).sortType,
        homePosts: []
      }
    }
    case UiActionTypes.INIT_SORT_COMMUNITY_POSTS: {
      return {
        ...state,
        communityPosts: []
      }
    }
    case PostActionTypes.LOAD_POSTS: {
      return {
        ...state, homePosts: (action as LoadPostsAction).posts.allIds
      }
    }
    case UiActionTypes.OPEN_ERROR_DIALOG: {
      const {message, title} = (action as OpenErrorDialogAction);
      return {
        ...state,
        error: true,
        errorMessage: message,
        errorTitle: title
      }
    }
    case UiActionTypes.CLOSE_ERROR_DIALOG: {
      return {
        ...state,
        error: false,
        errorMessage: "",
        errorTitle: ""
      }
    }
    case UiActionTypes.START_SPINNER: {
      return {
        ...state,
        spinner: true
      }
    }
    case UiActionTypes.STOP_SPINNER: {
      return {
        ...state,
        spinner: false
      }
    }
    case AppActionTypes.LOAD_DATA: {
      return {
        ...state, 
        homePosts: [...state.homePosts, ...(action as LoadDataAction).appState.posts.allIds],
        userPosts: [],
        communityPosts: []
      }
    }
    case AppActionTypes.LOAD_COMMUNITY_DATA: {
      return {
        ...state, communityPosts: (action as LoadCommunityDataAction).appState.posts.allIds
      }
    }
    case UserActionTypes.LOAD_USER_DATA: {
      return {
        ...state, userPosts: (action as LoadUserDataAction).appState.posts.allIds
      }
    }
    case PostActionTypes.LOAD_MORE_COMMUNITY_POSTS: {
      return {
        ...state, communityPosts: [...state.communityPosts, ...(action as LoadMorePostsAction).appState.posts.allIds]
      }
    }
    case PostActionTypes.LOAD_MORE_POSTS: {
      return {
        ...state, homePosts: [...state.homePosts, ...(action as LoadMorePostsAction).appState.posts.allIds]
      }
    }
    case UiActionTypes.SELECT_CHAT_COMMUNITY: {
      return {
        ...state,
        selectedChatCommunity: (action as SelectChatCommunityAction).community
      }
    }
    default: return state;
  }
}

export { reducer as uiReducer };

