import { AppActionTypes, AppState, LoadDataAction, LoadCommunityDataAction } from "./types";

export function fetchData() { return {type: AppActionTypes.FETCH_DATA }}

export function loadData(appState: AppState) : LoadDataAction {
  return { type: AppActionTypes.LOAD_DATA, appState: appState }
}

export function loadCommunityData(appState: AppState): LoadCommunityDataAction {
  return { type: AppActionTypes.LOAD_COMMUNITY_DATA, appState }
}