import * as saga from "redux-saga/effects";
import { API_URL } from "..";
import { apiFetch } from "../../services/auth";
import { openErrorDialog, stopSpinner } from "../ui/action";
import { joinCommunity, leaveCommunity } from "./action";
import { CommunityActionTypes, JoinCommunityAction, LeaveCommunityAction } from "./types";

export function* communitySaga() {
  yield saga.all([saga.fork(watchFetchRequests)]);
}

function* watchFetchRequests() {
  yield saga.takeEvery(CommunityActionTypes.INIT_JOIN_COMMUNITY, join);
  yield saga.takeEvery(CommunityActionTypes.INIT_LEAVE_COMMUNITY, leave);
} 

function* join(action: JoinCommunityAction) {
  const input = {
    UserId: action.user,
    CommunityId: action.community
  }
  const result = yield apiFetch('POST', API_URL + "User/JoinToCommunity", input);
  console.log("call");
  if(result.success) {
    yield saga.put(joinCommunity(action.community, action.user));
  }
  else {
    yield saga.put(openErrorDialog("", "Something went wrong. Please try again!"));
  }

  yield saga.put(stopSpinner());
}

function* leave(action: LeaveCommunityAction) {
  const input = {
    UserId: action.user,
    CommunityId: action.community
  }
  const result = yield apiFetch('POST', API_URL + "User/LeaveCommunity", input);

  if(result.success) {
    yield saga.put(leaveCommunity(action.community, action.user));
  }
  else {
    yield saga.put(openErrorDialog("", "Something went wrong. Please try again!"));
  }

  yield saga.put(stopSpinner());
}