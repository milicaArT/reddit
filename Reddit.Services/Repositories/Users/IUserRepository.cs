﻿using Reddit.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Reddit.Services.Repositories.Users
{
    public interface IUserRepository : INeo4JRepository<User>
    {
        Task<User> GetUserByUsernameAsync(string username, string password);
        Task<bool> CheckUserByUsernameAsync(string username, string password);
        Task JoinToCommunityAsync(long communityId, long userId);
        Task LeaveCommunityAsync(long communityId, long userId);
        Task<List<User>> FindManyUsersAsync(List<long> userIds);
    }
}
