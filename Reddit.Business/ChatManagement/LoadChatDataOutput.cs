﻿using Reddit.Domain.Entities;
using System.Collections.Generic;

namespace Reddit.Business.ChatManagement
{
    public class LoadChatDataOutput
    {
        public List<Message> Messages { get; set; }
        public List<User> Users { get; set; }
    }
}
