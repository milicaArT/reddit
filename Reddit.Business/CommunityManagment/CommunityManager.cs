﻿using Reddit.Domain.Entities;
using Reddit.Domain.Entities.Enums;
using Reddit.Services.Repositories;
using Reddit.Services.Repositories.Communities;
using System;
using System.Collections.Generic;

namespace Reddit.Business.CommunityManagment
{
    public class CommunityManager : ICommunityManager
    {
        private readonly IRedisRepository _redisRepository;
        private readonly ICommunityRepository _communityRepository;

        public CommunityManager(
            IRedisRepository redisRepository,
            ICommunityRepository communityRepository)
        {
            _redisRepository = redisRepository;
            _communityRepository = communityRepository;
        }

        public async void CheckCommunitiesInRedis()
        {
            List<Community> communities = await _redisRepository
                .GetAsync<List<Community>>(RedisKeys.COMMUNITIES).ConfigureAwait(false);

            List<Community> communitiesToAdd;

            if (communities == null)
            {
                await _redisRepository.SetAsync(RedisKeys.COMMUNITIES,
                    TestCommunities.AllCommunities).ConfigureAwait(false);

                communitiesToAdd = TestCommunities.AllCommunities;
            }
            else
            {
                List<long> communitiesInRedisIds = communities.ConvertAll(
                    new Converter<Community, long>(IdsConverter));

                communitiesToAdd = TestCommunities.AllCommunities.FindAll(
                    comm => !communitiesInRedisIds.Contains(comm.Id));

                if (communitiesToAdd.Count == 0)
                    return;

                communities.AddRange(communitiesToAdd);

                await _redisRepository.SetAsync(RedisKeys.COMMUNITIES,
                    communities).ConfigureAwait(false);
            }

            foreach (Community comm in communitiesToAdd)
            {
                await _communityRepository.CreateAsync(comm).ConfigureAwait(false);

                string key = comm.Title + "-" + RedisKeys.NEW_POSTS;
                await _redisRepository.SetAsync(key, new List<Post>()).ConfigureAwait(false);

                key = comm.Title + "-" + RedisKeys.BEST_POSTS;
                await _redisRepository.SetAsync(key, new List<Post>()).ConfigureAwait(false);

                key = comm.Title + "-" + RedisKeys.POPULAR_POSTS;
                await _redisRepository.SetAsync(key, new List<Post>()).ConfigureAwait(false);

                key = comm.Title + "-" + RedisKeys.COMMENTS;
                await _redisRepository.SetAsync(key, new List<Comment>()).ConfigureAwait(false);
            }

            await _redisRepository.SetAsync(RedisKeys.NEW_POSTS,
                new List<Post>()).ConfigureAwait(false);

            await _redisRepository.SetAsync(RedisKeys.BEST_POSTS,
                new List<Post>()).ConfigureAwait(false);

            await _redisRepository.SetAsync(RedisKeys.POPULAR_POSTS,
                new List<Post>()).ConfigureAwait(false);

            await _redisRepository.SetAsync(RedisKeys.COMMENTS,
                new List<Post>()).ConfigureAwait(false);

            await _redisRepository.SetAsync(RedisKeys.USERS,
                new List<Post>()).ConfigureAwait(false);
        }
        private long IdsConverter(IEntity entity)
        {
            return entity != null ? entity.Id : 0;
        }
    }
}
